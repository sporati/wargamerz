FROM bref/php-74-fpm-dev

# RUN apt-get update && apt-get install -y unzip curl git libicu-dev zlib1g-dev libzip-dev \
#    && docker-php-ext-install intl pcntl pdo_mysql zip

# COMPOSER
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

<?php
declare(strict_types = 1);

namespace App\Infrastructure\Persistence\DynamoDB\Repository;

use App\Domain\Entity\DiceRoll;
use App\Domain\Repository\DiceRollRepositoryInterface;
use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;

class DiceRollRepository implements DiceRollRepositoryInterface
{
    /**
     * @var DynamoDbClient
     */
    private $dynamoDbClient;

    /**
     * @var string
     */
    private $tableName;

    /**
     * DiceRollRepository constructor.
     *
     * @param DynamoDbClient $dynamoDbClient
     * @param string         $tableName
     */
    public function __construct(DynamoDbClient $dynamoDbClient, string $tableName)
    {
        $this->dynamoDbClient = $dynamoDbClient;
        $this->tableName      = $tableName;
    }

    /**
     * @param int $number
     *
     * @return DiceRoll[]
     * @throws \Exception
     */
    public function getLast($number = 10): array
    {
        return [
            new DiceRoll(),
            new DiceRoll(),
        ];
    }

    /**
     * @param DiceRoll $diceRoll
     *
     * @return DiceRoll
     */
    public function save(DiceRoll $diceRoll): ?DiceRoll
    {
        $marshaler = new Marshaler();

        $this->dynamoDbClient->putItem([
            'TableName'    => $this->tableName,
            'ReturnValues' => 'NONE',
            'Item'         => $marshaler->marshalItem([
                'DatePart'   => $diceRoll->getRolledAt()->format('Y-m-d'),
                'TimePart'   => $diceRoll->getRolledAt()->format('H:i:s.vP'),
                'DiceNumber' => $diceRoll->getNumber(),
                'DiceType'   => $diceRoll->getDiceType(),
                'Results'    => $diceRoll->getResults(),
            ]),
        ]);

        return $diceRoll;
    }
}

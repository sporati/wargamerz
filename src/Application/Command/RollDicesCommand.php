<?php
declare(strict_types = 1);

namespace App\Application\Command;

class RollDicesCommand
{
    /**
     * @var string
     */
    public $diceType;

    /**
     * @var int
     */
    public $number;
}

<?php
declare(strict_types = 1);

namespace App\Application\CommandHandler;

use App\Application\Command\RollDicesCommand;
use App\Domain\Entity\DiceRoll;
use App\Domain\Event\DiceRollCreated;
use App\Domain\Repository\DiceRollRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class RollDicesCommandHandler implements MessageHandlerInterface
{
    /**
     * @var MessageBusInterface
     */
    private $eventBus;

    /**
     * @var DiceRollRepositoryInterface
     */
    private $diceRollRepository;

    /**
     * Constructor.
     *
     * @param MessageBusInterface         $eventBus
     * @param DiceRollRepositoryInterface $diceRollRepository
     */
    public function __construct(MessageBusInterface $eventBus, DiceRollRepositoryInterface $diceRollRepository)
    {
        $this->eventBus           = $eventBus;
        $this->diceRollRepository = $diceRollRepository;
    }

    /**
     * @param RollDicesCommand $command
     *
     * @return DiceRoll|null
     * @throws \Exception
     */
    public function __invoke(RollDicesCommand $command)
    {
        $diceResults = [];
        for ($i = $command->number ; $i > 0 ; $i--) {
            switch ($command->diceType) {
                case 'd3':
                    $diceResults[] = random_int(1, 3);
                    break;

                case 'd6':
                    $diceResults[] = random_int(1, 6);
                    break;
            }
        }

        $diceRoll = new DiceRoll($command->number, $command->diceType, $diceResults);
        $diceRoll = $this->diceRollRepository->save($diceRoll);

        $this->eventBus->dispatch(new DiceRollCreated());

        return $diceRoll;
    }
}

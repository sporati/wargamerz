<?php
declare(strict_types = 1);

namespace App\Domain\EventSubscriber;

use App\Domain\Event\DiceRollCreated;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class TestDiceRollCreatedEventSubscriber implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param DiceRollCreated $event
     */
    public function __invoke(DiceRollCreated $event)
    {
        // TODO: Implement __invoke() method.
        $this->logger->info('TestDiceRollCreatedEventSubscriber::__invoke()');
    }
}

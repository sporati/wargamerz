<?php
declare(strict_types = 1);

namespace App\Domain\Entity;

class DiceRoll
{
    /**
     * @var int
     */
    private $number;

    /**
     * @var string
     */
    private $diceType;

    /**
     * @var array
     */
    private $results = [];

    /**
     * @var \DateTime
     */
    private $rolledAt;

    /**
     * DiceRoll constructor.
     *
     * @param int            $number
     * @param string         $diceType
     * @param array          $results
     * @param \DateTime|null $rolledAt
     *
     * @throws \Exception
     */
    public function __construct(int $number, string $diceType, array $results, \DateTime $rolledAt = null)
    {
        $this->number   = $number;
        $this->diceType = $diceType;
        $this->results  = $results;
        $this->rolledAt = $rolledAt ?? new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getDiceType(): string
    {
        return $this->diceType;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @return \DateTime
     */
    public function getRolledAt(): \DateTime
    {
        return $this->rolledAt;
    }
}

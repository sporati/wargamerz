<?php
declare(strict_types = 1);

namespace App\Domain\Repository;

use App\Domain\Entity\DiceRoll;

interface DiceRollRepositoryInterface
{
    /**
     * @param int $number
     *
     * @return DiceRoll[]
     */
    public function getLast($number = 10): array;

    /**
     * @param DiceRoll $diceRoll
     *
     * @return DiceRoll
     */
    public function save(DiceRoll $diceRoll): ?DiceRoll;
}

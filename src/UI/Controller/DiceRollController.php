<?php
declare(strict_types = 1);

namespace App\UI\Controller;

use App\Application\Command\RollDicesCommand;
use App\Domain\Repository\DiceRollRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\SerializerInterface;

class DiceRollController
{
    /**
     * @var DiceRollRepositoryInterface
     */
    private $diceRollRepository;

    /**
     * @var MessageBusInterface
     */
    private $commandBus;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * DiceRollController constructor.
     *
     * @param DiceRollRepositoryInterface $diceRollRepository
     * @param MessageBusInterface         $commandBus
     * @param SerializerInterface         $serializer
     */
    public function __construct(
        DiceRollRepositoryInterface $diceRollRepository,
        MessageBusInterface $commandBus,
        SerializerInterface $serializer
    ) {
        $this->diceRollRepository = $diceRollRepository;
        $this->commandBus         = $commandBus;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/dice-rolls", name="api_dicerolls_create", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $rollDicesCommand = $this->serializer->deserialize(
                $request->getContent(),
                RollDicesCommand::class,
                'json'
            );
        } catch (NotNormalizableValueException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        // @TODO: validate command (diceType, number, ...)

        $rollDice = $this->commandBus
            ->dispatch($rollDicesCommand)
            ->last(HandledStamp::class)
            ->getResult();

        dump($rollDice);

        return new JsonResponse(
            $this->serializer->serialize($rollDice, 'json', ['groups' => 'api']),
            200,
            [],
            true
        );
    }

    /**
     * @Route("/dice-rolls", name="api_dicerolls_list", methods={"GET"})
     */
    public function list()
    {
        return new JsonResponse(
            $this->diceRollRepository->getLast()
        );
    }
}

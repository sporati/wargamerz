DOCKER_COMPOSE=cd config/docker && docker-compose

build: ## Build Docker dev container
build:
	$(DOCKER_COMPOSE) build --pull --no-cache

up: ## Launch Docker dev container
up:
ifndef NO_GIT
	git config core.hooksPath config/git/hooks || true
endif
	$(DOCKER_COMPOSE) up -d --force-recreate
	sleep 10
	make dynamodb-create-tables
	$(DOCKER_COMPOSE) exec -T php composer install --no-cache
	# make build-database
#ifndef NO_YARN
#	$(DOCKER_COMPOSE) exec -T  php yarn install
#	$(DOCKER_COMPOSE) exec -T  php yarn encore dev
#endif

bash: ## Open bash on Docker dev container
bash:
	$(DOCKER_COMPOSE) exec  php $(if $(CMD), ${CMD}, /bin/bash)

down: ## Stop Docker dev container
down:
	$(DOCKER_COMPOSE) down

dynamodb-create-tables: ## Create DynamoDB dev tables
dynamodb-create-tables:
	$(DOCKER_COMPOSE) run --rm awscli --region "eu-west-1" --endpoint-url "http://localstack:4569" dynamodb create-table --table-name wargamerz_dev_dice-rolls --attribute-definitions '[{"AttributeName":"DatePart","AttributeType":"S"},{"AttributeName":"TimePart","AttributeType":"S"}]' --key-schema '[{"AttributeName":"DatePart","KeyType":"HASH"},{"AttributeName":"TimePart","KeyType":"RANGE"}]' --provisioned-throughput '{"ReadCapacityUnits":100,"WriteCapacityUnits":20}'

dynamodb-scan-table: ## Scan DynamoDB dev table. Parameter TABLE_NAME required.
dynamodb-scan-table:
	$(DOCKER_COMPOSE) run --rm awscli --region "eu-west-1" --endpoint-url "http://localstack:4569" dynamodb scan --table-name $(TABLE_NAME)

build-database: ## Drop and recreate database, and load fixtures.
build-database:
	$(DOCKER_COMPOSE) exec -T  php ./bin/console doctrine:schema:drop --full-database --env=dev --force
	$(DOCKER_COMPOSE) exec -T  php ./bin/console doctrine:migrations:migrate --env=dev  --no-interaction
	$(DOCKER_COMPOSE) exec -T  php ./bin/console doctrine:fixtures:load --env=dev --no-interaction

build-database@test: ## Drop and recreate database, and load fixtures.
build-database@test:
	$(DOCKER_COMPOSE) exec -T  -e APP_ENV=test php ./bin/console doctrine:schema:drop --full-database --env=test --force
	$(DOCKER_COMPOSE) exec -T  -e APP_ENV=test php ./bin/console doctrine:migrations:migrate --env=test  --no-interaction

run-tests-phpunit: ## Run PhpUnit tests. Parameter FILTER can be used to launch specific tests.
run-tests-phpunit:
	$(DOCKER_COMPOSE) exec -T  -e APP_ENV=test php ./bin/phpunit $(if $(FILTER), --filter=${FILTER})

run-tests-behat: ## Run all Behat tests. Parameters PROFILE, TAGS can be used.
run-tests-behat:
	$(DOCKER_COMPOSE) exec -T  -e APP_ENV=test php ./vendor/bin/behat $(if $(TAGS), --tags=${TAGS}) --strict --no-snippets

run-phpcs: ## Run PHPCS to check coding standards.
run-phpcs:
	$(DOCKER_COMPOSE) exec -T  php ./vendor/bin/phpcs --config-set installed_paths vendor/escapestudios/symfony2-coding-standard
	$(DOCKER_COMPOSE) exec -T  php ./vendor/bin/phpcs src/ tests/

run-phpstan: ## Run PHPStan to find errors in code.
run-phpstan:
	$(DOCKER_COMPOSE) exec -T  php ./vendor/bin/phpstan analyse src/ --level max --no-progress

run-tests: ## Run all tests and check coding standards.
run-tests:
	make build-database@test
	make run-tests-phpunit
	make run-tests-behat

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
